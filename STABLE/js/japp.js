/* globals $ */
"use strict";

$(function() {

// ORDERS

  // Attiva il campo per il ricalcolo dell'ordine
  var inputOrder = $("#ordinetesto");

  inputOrder.on("keyup", function() {

		var ordini = [];

		// recupera il valore dell'input
		var fv = $(this).val();

    // Array nominativi
    //var persone = [];

		var url = new URL(window.location.href);
		var nameInGet = url.searchParams.get("name");

		// Crea la lista dell'ordine per persona/e
		var isSingleOrder = (typeof(nameInGet) !== 'undefined') && (nameInGet !== null);

		var ordPersona = nameInGet;
		var ordArray = fv.split(" ")
										 .filter( value => value.length !== 0 && value.trim() );
		ordini.push({ persona: ordPersona, ordine: ordArray });
		isSingleOrder = true;

    // Se è tutto valido, salva l'ordine
    //if ( ! $('#ordinetesto').hasClass('errorValidate') ) {
      var newOrdini = ordini.slice();
      saveData(newOrdini);
    //}

		// reset array orders
    ordini = [];
  });

  inputOrder.trigger("keyup");

// MENU

  // Assembla la tabella del menu
	var menulista = $("#lista");
  var menuTable = $("<ul class='order-list'>");
  var title = '';

  $.each(menu, function(i, v) {
    if( title != v.categoria ) {
      title = v.categoria;
      menuTable.append("<li class='categoria'><h2>"+v.categoria+"</h2></li>");
    }
    var classRow = (v.nome[0] === '*') ? 'error' : '' ;
    var mRow = $("<li>");
    mRow.html('<strong class="list-num">'+i+'</strong> <span class="list-name '+classRow+'">'+v.nome+'</span>');
    menuTable.append(mRow);
  });
  menulista.append(menuTable);

});
