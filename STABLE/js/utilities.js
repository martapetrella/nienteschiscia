/* globals $ */
"use strict";

// Impedisce troppe richieste ajax consecutive
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

// Chiamata al salvataggio dell'ordine
var saveData = debounce(function(newOrdini) {

	let currUrl = new URL(window.location.href);
	var baseUrl = "ordini/crud.php";
	let menu = currUrl.searchParams.get("menu") ? "&menu="+currUrl.searchParams.get("menu") : '';
	let name = currUrl.searchParams.get("name") ? "&name="+currUrl.searchParams.get("name") : '';

	var params = "?ordine="+JSON.stringify( newOrdini )+menu+name;
	var url = baseUrl+params;

  $.ajax({
    url: url,
    context: document.body,
		success: function(result){
			//console.warn(result);
			createTabella(result);
	  }
  });
}, 100);
