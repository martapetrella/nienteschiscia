function printMenu(menu) {
  // Assembla la tabella del menu
  var menulista = $("#lista");
  var menuTable = $("<ul class='order-list'>");
  var title = '';

  $.each(menu.list, function(i, v) {
    // stampa solo se la categoria non inizia con *
    if( v.categoria[0] !== '*' ) {
      if( title != v.categoria ) {
        title = v.categoria;
        menuTable.append("<li class='categoria'><h2>"+v.categoria+"</h2></li>");
      }
      if( v.nome[0] !== '*' ) {
        // v.nome[0] !== '*'
        var classRow = (v.nome[0] === '*') ? 'error' : '' ;
        var mRow = $("<li id='listItem_"+i+"' class='list-item'>");
        var rowContent = '';
        rowContent += '<strong class="list-num">'+i+'</strong>';
        rowContent += '<span class="list-name '+classRow+'">'+v.nome;
        if( v.desc && classRow != 'error' ) rowContent += '<br/><small>'+v.desc+'</small>';
        rowContent += '</span>';
        rowContent += '<span class="list-select">';
        rowContent += '<button data-value="'+i+'" class="list-btn list-btn-remove" disabled>-</button>';
        rowContent += '<input  data-value="'+i+'"class="list-qty" readonly value="0" type="text">';
        rowContent += '<button data-value="'+i+'" class="list-btn list-btn-add">+</button>';
        rowContent += '</span>';
        mRow.html(rowContent);
        menuTable.append(mRow);
      }
    }
  });
  menulista.append(menuTable);
  $('#phonelink').attr('href','tel:'+menu.phone);
  $('#phonelabel').html(menu.phone);
}
