/* globals $ */
"use strict";

$(function() {

  if( window.location.hash && window.location.hash != '' ) {
    $('.tab-button, .tab-content').removeClass('is-active');
    $('a[href*="'+window.location.hash+'"], '+window.location.hash).addClass('is-active');
  }
  $('a[href*="#inserimento"], a[href*="#riassunto"]').click(function(e){
		e.preventDefault();
    $('.tab-button, .tab-content').removeClass('is-active');
    $(this).addClass('is-active');
    $($(this).attr('href')).addClass('is-active');
		$( "body" ).scrollTop( 0 );
  });

});
