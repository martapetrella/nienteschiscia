/* globals $ */
"use strict";

$(function() {

  var inputOrder = $("#ordinetesto");

// MENU
  printMenu(menu);

// ORDERS
  // Attiva il campo per il ricalcolo dell'ordine
  inputOrder.on("keyup", function() {
    var newOrdini = updateOrdini();
    saveData(newOrdini);
    // imposto a zero tutte lo qty
    $('.list-item').each(function(){
      $(this).find('.list-qty').val(0);
      $(this).find('.list-btn-remove').attr('disabled','disabled');
      $(this).addClass('is-hidden');
    });
    // ciclo su array ordini e trovo ID corrispondente nella tabella del menu
    var ordineNumbers = newOrdini[0].ordine;
    ordineNumbers.map(function(i){
      var curr = $('#listItem_'+i)
      var qty = curr.find('.list-qty');
      qty.val( parseInt( qty.val(),10 ) + 1 );
      // se qty > 0 tolgo disable. se è zero lo lascio
      curr.find('.list-btn-remove').removeAttr('disabled');
      curr.removeClass('is-hidden');
    });
  }).trigger("keyup");

  $('.list-btn').on('click',function(){
    var btn = $(this);
    var removeBtn = btn.closest('.list-item').find('.list-btn-remove');
    var listItem = btn.closest('.list-item');
    var listValue = listItem.find('.list-qty');
    var preNum = parseInt( listValue.val() );
    var postNum;
    // aggiunge o sottrae nell'input del singolo list item
    if( btn.hasClass('list-btn-remove') ){
      postNum =  ( preNum > 0 ) ? ( preNum - 1 ) : 0;
    } else {
      postNum =  preNum + 1;
    }
    listValue.val( postNum );
    // disabilita il bottone remove se l'item è a zero.
    if( parseInt( postNum ) > 0 ) {
      removeBtn.removeAttr('disabled');
      listItem.removeClass('is-hidden');
    } else {
      removeBtn.attr('disabled','disabled');
      listItem.addClass('is-hidden');
    }

    var newStr = '';
    $('.list-item').each(function(){
      var input = $(this).find('.list-qty');
      var n = input.data('value');
      var v = input.val();
      if(v>0) {
        var i;
        for (i = 0; i < v; i++) {
          newStr += n+' ';
        }
      }
    });
    inputOrder.val(newStr);
    var newOrdini = updateOrdini();
    saveData(newOrdini);
    // recupera il valore input, lo trasforma in array e lo ordina
    var ordineNumbers = newOrdini[0].ordine.sort();
  });

  function updateOrdini() {
    var ordini = [];
    // Recupera la persona corrente
    var url = new URL(window.location.href);
    var nameInGet = url.searchParams.get("name");
    // Crea la lista dell'ordine per persona
    var ordPersona = nameInGet;
    ordini.push({ persona: ordPersona, ordine: getOrdArray() });
    // Salva l'ordine
    return ordini.slice();
  }

  function getOrdArray() {
    // Recupera il valore dell'input, fa lo split poi pulisce da eventuali valori vuoti col trim
    return inputOrder.val().split(" ").filter( value => value.length !== 0 && value.trim() )
  }

});
