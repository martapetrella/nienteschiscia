// CREA LA LISTA PER PIATTI PARTENDO DA QUELLA PER PERSONE

function createPiattiList(ordini,persone) {
	var piattiList = {};
	var iserror = false;
	var errorStr = '';
	// cicla gli ordini
	$.each(ordini, function(i, v) {
		// cicla i numeri dell'ordine
		$.each(v.ordine, function(ind, num) {
			var nome;
			var riga = {};
			//console.log( menu[num] );

			if( menu.list[num] !== undefined ) {
				if( piattiList[num] === undefined ){
					riga.Numero = num;
					riga.Nome = menu.list[num].nome;
					riga.Porzioni = 1;
					$.each(persone, function(y, p) {
						nome = ( isNaN(p) ) ? p : p+' ' ; // forza la stringa
						riga[nome] = (p == v.persona) ? 1 : 0;
					});
					piattiList[num] = riga;
				} else {
					piattiList[num].Porzioni = piattiList[num].Porzioni + 1;
					nome = ( isNaN(v.persona) ) ? v.persona : v.persona+' ' ; // forza la stringa
					piattiList[num][nome] = piattiList[num][nome] + 1;
				}
			} else {
				iserror = true;
				errorStr += '"'+num+"\" nell'ordine di "+v.persona+" non è valido. <br />";
			}
		});

		if( iserror ) {
			$('#ordinetesto').addClass('errorValidate');
			$('#errorMsg').show().html(errorStr);
		} else {
			$('#ordinetesto').removeClass('errorValidate');
			$('#errorMsg').hide().html('');
		}

	});
	return piattiList;
}

// CREA LA TABELA IN BASE AI PIATTI
function createTabella( string ){
  var obj = JSON.parse(string);
  var ordini = obj.ordine;
  var persone = [];
	var numorders = 0;
	var ordTable = '';
	var container = $("#ordine");
	var errorMsg = $('#errorMsg');
	container.empty();
  if( ordini ) {
    $.each(ordini, function(i, v) {
      if( v.ordine.length > 0 ) {
				persone.push(v.persona);
				numorders++;
			}
    });
		if( numorders == 0 ) {
			errorMsg.show().html( errorMsg.data('msgdefault') );
		} else {
			errorMsg.hide().html('');
	    var piattiList = createPiattiList(ordini,persone);
	    // Prepara l'oggetto per il nuovo fill della tabella
	    // Assembla la tabella
	    ordTable = $("<table class='orderTable'>");
	    // Crea l'header
	    var tHead = $("<tr>");
	    var classeTh = "";
	    ordTable.append(tHead);
	    var first = piattiList[Object.keys(piattiList)[0]];
	    if( piattiList !== undefined && first !== undefined ) {
	      $.each(first, function(indice, dato) {
	        var name = '<span class="desktop">'+indice+'</span><span class="mobile">'+indice.substring(0,3) + '.</span>';
	        if (indice=='Numero') name = '#';
	        if (indice=='Porzioni') name = 'TOT.';
					if (indice=='Nome') name = 'Piatto';
	        tHead.append($("<th>").addClass(classeTh).html(name));
	      });
	      // Mostra i dati nella tabella
	      $.each(piattiList, function(i, datiriga) {
	        var tRow = $("<tr>");
	        var tRowClass = '';
	        $.each(datiriga, function(indice, daticella) {
	          var classe = "",
	          dato = daticella;
	          if (indice == "Numero" || indice == "Porzioni") {
	            classe = "evidente";
	          } else if (indice == "Nome") {
	            classe = "evidente nome";
	            if (daticella[0] === '*') {
	              tRowClass = 'error';
	              dato = daticella.substring(1);
							}
	          } else {
	            if (daticella) {
	              dato = daticella;
	              classe = 'checked';
	            } else {
	              dato = '';
	            }
	          }

	          tRow.append($("<td>").addClass(classe).html(dato));
	        });
	        if (tRowClass !== '') {
	          tRow.addClass(tRowClass);
	        }
	        ordTable.append(tRow);
	      });
	    }
		}
    // Inserimento dell'ordine
    container.append(ordTable);

  }
}
