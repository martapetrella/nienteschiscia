<?php
include '../utilities.php';

$getName = $_GET['name'];
$getMenu = $_GET['menu'] ? $_GET['menu'] : '';
$getOrdine = json_decode($_GET['ordine'])[0];

$filename = 'ordine_'.( ($getMenu!='') ? $getMenu.'_' : '' ).date('Ymd').'.json';
$filenameExist = file_exists($filename);

// Se il file dell'ordine esiste
if( $filenameExist ) {

  $data = json_decode( file_get_contents($filename) );
  $dataOrder = $data->ordine;

  $currentName = false;
  $currentNameIndex = false;
  $index = 0;
  foreach( $dataOrder as $k => $v ) {
    if( !$control && $v->persona == $getName ) {
      $currentName = $v;
      $currentNameIndex = $index;
      $control = true;
    }
    $index++;
  }

  // se non c'è il nome, l'index è il numero totale degli elementi (quindi il prossimo)
  if( ! $currentName ) {
    $currentNameIndex = $index;
  }

  $dataOrder[$currentNameIndex] = $getOrdine;

  $obj = [
    //"status" => "file updated",
    "menu"  => $getMenu,
    "ordine" => $dataOrder,
  ];

} else {
  // ..se no scrivo tutto il file da zero.
  $obj = [
    //"status" => "file create",
    "menu"  => $getMenu,
    "ordine" => $dataOrder,
  ];
}

$result = json_encode($obj);

$fp = fopen($filename, 'w');
fwrite($fp, ''); // clear
fwrite($fp, $result); // rewrite
fclose($fp);

echo $result;
?>
