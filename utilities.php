<?php

define("PRE_STYLE", "font-size:10px;text-align:left;background:#eee;color:#000;border:black solid 1px;padding:5px;max-height:90vh;max-width:90vw;overflow:auto;margin-left:auto;margin-right:auto;" );

define("PRE_STYLE_JUMP_HEADER", "margin-top:110px;margin-bottom:15px auto;" );
define("PRE_STYLE_HR", "margin:5px 0;paddign:0;height:0;display:block;border:none;border-bottom:black solid 1px;" );

function pre_var_dump($val,$title=false,$height=false,$width=false,$float='none',$jumpHeader=false) {
  echo "<pre style='".PRE_STYLE;
  if($jumpHeader) echo PRE_STYLE_JUMP_HEADER;
  if($height) echo "height:{$height};max-height:{$height};overflow:scroll;";
  if($width) echo "width:{$width};max-width:{$width};word-break:break-all;overflow:scroll;";
  echo "float:{$float};";
  echo " '>";
  if( $title ) echo "<strong style='font-size:1.2em'>".$title."</strong><hr style='".PRE_STYLE_HR."' />";
  echo var_dump($val);
  echo "</pre>";
}

?>
