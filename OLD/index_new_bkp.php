<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>NienteSchiscia</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Ordine Jappo">
  <meta name="author" content="Dave">
  <link rel="stylesheet" type="text/css" href="style.css?v=<?= date('Ymdhhmmss') ?>" />
  <script src="jquery.min.js"></script>
  <script src="<?php
    $menuFolder = 'menu/';
    $getMenu = $menuFolder.$_GET['menu'].'.js';
    if( file_exists ( $getMenu )  ) {
      echo $getMenu;
    } else {
      echo $getMenu = $menuFolder.'ginza'.'.js'; // momentaneo
      //echo 'menu.js';
    }
  ?>"></script>
  <!-- script>var ordini = <?php //include 'ordini/ordine_'.date('Ymd').'.json' ?>; console.log('ordini',ordini);</script-->
  <script src="japp.js?v=<?= date('Ymdhhmmss') ?>"></script>
</head>
<body>
  <header>
    <textarea id="ordinetesto" placeholder="SCRIVI&nbsp;L'ORDINE (es&nbsp;Nome:&nbsp;1&nbsp;2&nbsp;3)"><?php
      //$menu = ( $_GET['menu'] ) ? $_GET['menu'].'_' : '' ;
      $filename = 'ordini/ordine_'.date('Ymd').'.json';
      if(file_exists($filename)){
        $menu = ( $_GET['menu'] ) ? $_GET['menu'].'_' : '' ;
        $data = json_decode( file_get_contents($filename) );
        if($data) {
          foreach ($data as &$value) {
              echo $value->persona.': ';
              foreach ($value->ordine as &$v) {
                echo $v.' ';
              }
              echo "\n";
          }
        }
      }
    ?></textarea>
  </header>
  <nav>
    <a href="#inserimento" class="tab-button tab-button--img is-active">
      <img src="imgs/piatto.png" />
    </a>
    <a href="#riassunto" class="tab-button tab-button--img">
      <img src="imgs/coperto.png" />
    </a>
    <!--a class=" tab-button tab-button--img phone" href="tel:0248011681">
      <img src="imgs/phone.jpg" />
    </a-->
  </nav>
  <div id="main">
    <div id="inserimento" class="tab-content is-active">
      <div id="menu">
        <div id="lista"></div>
        <p class="error">*piatti non consentiti a pranzo</p>
      </div>
    </div>
    <div id="riassunto" class="tab-content">
      <a class="btn-phone" href="tel:0248011681">
        <img src="imgs/phone.jpg" />
        02 48011681
      </a>
      <div id="ordine"></div>
    </div>
  </div>
</body>
</html>
