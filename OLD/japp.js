/* globals $ */
"use strict";

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

var saveData = debounce(function(newOrdini) {
  $.ajax({
    url: "ordini/crud.php?ordine="+JSON.stringify( newOrdini )+"&menu=ginza",
    context: document.body
  }).done(function() {
    //console.log('AjaxCall Done!');
  }).always(function() {
    //console.log( 'AjaxCall end!', newOrdini );
  });
}, 250);

$(function() {

  // TOGGLE TABS
  if( window.location.hash && window.location.hash != '' ) {
    $('.tab-button, .tab-content').removeClass('is-active');
    $('a[href*="'+window.location.hash+'"], '+window.location.hash).addClass('is-active');
  }
  $('a[href*="#inserimento"], a[href*="#riassunto"]').click(function(e){
		//e.preventDefault();
    $('.tab-button, .tab-content').removeClass('is-active');
    $(this).addClass('is-active');
    $($(this).attr('href')).addClass('is-active');
		$( "body" ).scrollTop( 0 );
  });

  // ORDERS

  // Attiva il campo per il ricalcolo dell'ordine
  var ordini = [];
  var ord = $("#ordinetesto") //.html(); // TODO: qui caricare json giorno corrente
  var tpers = $("#totPersone");
  var ordsunto = $("#ordine");
  var menulista = $("#lista");

  ord.on("keyup", function() {

    var fv = $(this).val();
    // Array nominativi
    var persone = [];
    // Array ordine per persona
    $.each(fv.split(/\n/), function(i, v) {
      var riga = v.split(":");
      if(riga.length == 2){
        var ordPersona = riga[0];
        var ordArray = riga[1].split(" ").filter( value => value.length !== 0 && value.trim() ); // .replace(/[^0-9\-]/g, "-").split("-");
        persone.push( ordPersona );
        ordini.push({ persona: ordPersona, ordine: ordArray });
      }
    });

    // Numero di persone
    tpers.html(ordini.length);
    // Prepara l'oggetto per il fill della tabella
    ordsunto.empty();
    //if (ordini.length > 0) {
      var tabledata = {};
      var univoci = [];
      // cicla gli ordini
      $.each(ordini, function(i, v) {
        // cicla i numeri dell'ordine
        $.each(v.ordine, function(ind, num) {
          var nome;
          var riga = {};
          if( menu[num] !== undefined ) {
            if( tabledata[num] === undefined ){
              riga.Numero = num;
              riga.Nome = menu[num].nome;
              riga.Porzioni = 1;
              $.each(persone, function(y, p) {
                nome = ( isNaN(p) ) ? p : p+' ' ; // forza la stringa
                riga[nome] = (p == v.persona) ? 1 : 0;
              });
              tabledata[num] = riga;
            } else {
              tabledata[num].Porzioni = tabledata[num].Porzioni + 1;
              nome = ( isNaN(v.persona) ) ? v.persona : v.persona+' ' ; // forza la stringa
              tabledata[num][nome] = tabledata[num][nome] + 1;
            }
            $('#ordinetesto').removeClass('errorValidate');
          } else {
            $('#ordinetesto').addClass('errorValidate');
          }
        });
      });

      // SAVE ORDER ONLY IF IS VALID
      if ( ! $('#ordinetesto').hasClass('errorValidate') ) {
        var newOrdini = ordini.slice();
        saveData(newOrdini);
      }

      ordini = []; // reset array orders

      // Assembla la tabella
      var ordTable = $("<table class='orderTable'>");
      // Crea l'header
      var tHead = $("<tr>");
      var classeTh = "";
      ordTable.append(tHead);
      var first = tabledata[Object.keys(tabledata)[0]];
      if( tabledata !== undefined && first !== undefined ) {
        $.each(first, function(indice, dato) {
          var name = indice
          if (indice=='Numero') name = '#';
          if (indice=='Porzioni') name = 'Totale';
          if (indice=='Nome') name = 'Piatto';
          tHead.append($("<th>").addClass(classeTh).html(name));
        });
        // Mostra i dati nella tabella
        $.each(tabledata, function(i, datiriga) {
          var tRow = $("<tr>");
          var tRowClass = '';
          $.each(datiriga, function(indice, daticella) {
            var classe = "",
            dato = daticella;
            if (indice == "Numero" || indice == "Porzioni") {
              classe = "evidente";
            } else if (indice == "Nome") {
              classe = "evidente nome";
              if (daticella[0] === '*') {
                tRowClass = 'error';
                dato = daticella.substring(1);
              }
            } else {
              if (daticella) {
                dato = daticella;
                classe = 'checked';
              } else {
                dato = '';
              }
            }

            tRow.append($("<td>").addClass(classe).html(dato));
          });
          if (tRowClass !== '') {
            tRow.addClass(tRowClass);
          }
          ordTable.append(tRow);
        });
      }
      // Inserimento dell'ordine
      ordsunto.append(ordTable);
    //}

  });

  ord.trigger("keyup");

  // Assembla la tabella del menu
  var menuTable = $("<ul class='order-list'>");
  var title = '';

  $.each(menu, function(i, v) {
    if( title != v.categoria ) {
      title = v.categoria;
      menuTable.append("<li class='categoria'><h2>"+v.categoria+"</h2></li>");
    }
    var classRow = (v.nome[0] === '*') ? 'error' : '' ;
    var mRow = $("<li>");
    mRow.html('<strong class="list-num">'+i+'</strong> <span class="list-name '+classRow+'">'+v.nome+'</span>');
    menuTable.append(mRow);
  });
  menulista.append(menuTable);

});
