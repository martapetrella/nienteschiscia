<?php header("Access-Control-Allow-Origin: *"); ?>

[
  { "id" : "1",
    "prod" : "2022",
    "lang" : "it",
    "code" : "bluron",
    "name" : "Bluronica",
    "price" : "10.00",
    "oldprice" : "20.00",
    "currency" : "€",
    "claim" : "Lorem ipsum dolor sit amet",
    "description" : "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "faqs" : [
      {
        "faq_prio" : "2",
        "faq_question" : "Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        "faq_answer" : "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        "faq_votes" : ""
      }, {
        "faq_prio" : "1",
        "faq_question" : "Ut enim ad minim veniam, quis nostrud exercitation",
        "faq_answer" : "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "faq_votes" : ""
      }
    ],
    "boxes" : [
      {
        "box_prio" : "2",
        "box_title" : "Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        "box_text" : "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        "box_img" : ""
      }, {
        "box_prio" : "1",
        "box_title" : "Ut enim ad minim veniam, quis nostrud exercitation",
        "box_text" : "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "box_img" : "",
        "box_cta" : ""
      }
    ]
  }, { "id" : "2",
    "prod" : "htf_footfix",
    "lang" : "it",
    "code" : "footx",
    "name" : "FootFix",
    "price" : "10.00",
    "oldprice" : "20.00",
    "currency" : "€",
    "claim" : "Lorem ipsum dolor sit amet",
    "description" : "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "faqs" : [
      {
        "faq_prio" : "2",
        "faq_question" : "Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        "faq_answer" : "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        "faq_votes" : ""
      }, {
        "faq_prio" : "1",
        "faq_question" : "Ut enim ad minim veniam, quis nostrud exercitation",
        "faq_answer" : "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "faq_votes" : ""
      }
    ],
    "boxes" : [
      {
        "box_prio" : "2",
        "box_title" : "Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        "box_text" : "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        "box_img" : ""
      }, {
        "box_prio" : "1",
        "box_title" : "Ut enim ad minim veniam, quis nostrud exercitation",
        "box_text" : "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "box_img" : "",
        "box_cta" : ""
      }
    ]
  }
]
