<?php
include 'utilities.php';

$name = $_GET['name'];

// recupero il file del menu
$menuPath = '';
$menuFolder = 'menu/';
$getMenu = $menuFolder.$_GET['menu'].'.js';
if( file_exists ( $getMenu )  ) {
  $menuPath = $getMenu;
} else {
  $menuPath = $menuFolder.'ginza'.'.js'; // momentaneo
}

// scrivo l'ordine
$ordineStr = '';
$menu = ( $_GET['menu'] ) ? $_GET['menu'].'_' : '' ;
$filename = 'ordini/ordine_'.$menu.date('Ymd').'.json';
if(file_exists($filename)){
  $data = json_decode( file_get_contents($filename) );
  $order = $data->ordine;
  if($order) {
    if($name) {
      foreach ($order as $value) {
        if($name==$value->persona){
          foreach ($value->ordine as $v) {
            $ordineStr .= $v.' ';
          }
        }
      }
    } else {
      foreach ($order as $value) {
          $ordineStr .= $value->persona.': ';
          foreach ($value->ordine as $v) {
            $ordineStr .= $v.' ';
          }
          $ordineStr .= "\n";
      }
    }
  }
}

$versioning = date('Ymdhhmmss'); // svuota la cache
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>NienteSchiscia</title>
  <meta name="description" content="Ordine Jappo">
  <meta name="author" content="Dave&Marta">
  <link rel="stylesheet" type="text/css" href="style.css?v=<?= $versioning ?>" />
  <!--style>.errorValidate {border:red solid 2px !important; }</style-->
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <script src="js/jquery.min.js"></script>
  <script src="<?= $menuPath; ?>"></script>
  <script src="js/utilities.js?v=<?= $versioning ?>"></script>
  <script src="js/tabs.js?v=<?= $versioning ?>"></script>
  <script src="js/table.js?v=<?= $versioning ?>"></script>
  <script src="js/menu.js?v=<?= $versioning ?>"></script>
  <script src="js/japp.js?v=<?= $versioning ?>"></script>
  <!-- FAVICONS -->
  <link rel="icon" href="favicon/favicon.png" sizes="16x16" type="image/png">
  <link rel="manifest" href="favicon/manifest.json?v=<?= $versioning ?>">
  <!-- FULLSCREEN MODE -->
  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black" />

  <link rel="icon" href="favicon/favicon.png" sizes="16x16" type="image/png">
  <link rel="apple-touch-icon" sizes="57x57" href="favicon/icons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="favicon/icons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="favicon/icons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="favicon/icons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="favicon/icons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="favicon/icons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="favicon/icons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="favicon/icons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/icons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="favicon/icons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon/icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="favicon/icons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon/icons/favicon-16x16.png">


</head>
<body class="<?php if(!$name) echo 'no-name' ?>">
  <header>
    <select class="select-nomi" onchange="location = '?name='+this.value+'#inserimento';">
      <option value=""></option>
      <?php $names = [ 'Dave','Ric','Drew','Marta','Laura','Beatrice','Fab','ospite' ];
      foreach ($names as $n) { ?>
        <option value="<?= $n ?>" <?php if( $name == $n ) echo 'selected' ?>><?= $n ?></option>
      <?php } ?>
    </select>
    <?php if($name) { ?>
      <input type="text" id="ordinetesto" value="<?= $ordineStr ?>" />
    <?php } else { ?>
    <p>Scegli un nome e ordina!</p>
  <?php } ?>
  </header>
  <div id="main">
    <div id="inserimento" class="tab-content is-active">
      <button onclick="$('body').toggleClass('read-mode')" class="toggle-mode">
        <span class="label-read">Ordine</span>
        <span class="label-write">Lista</span>
      </button>
      <div id="menu">
        <div id="lista"></div>
        <!--p class="error">*piatti non consentiti a pranzo</p-->
      </div>
    </div>
    <div id="riassunto" class="tab-content">
      <a id="phonelink" class="btn-phone" href="">
        <img src="imgs/phone.jpg" />
        <span id="phonelabel"></span>
      </a>
      <div id="errorMsg"
          class="error-msg"
          style="display:none"
          data-msgdefault="Scrivi i numeri dell'ordine sotto il tuo nome">
          Scrivi i numeri dell'ordine sotto il tuo nome
      </div>
      <div id="ordine"></div>
    </div>
  </div>
  <nav>
    <a href="#inserimento" class="tab-button tab-button--img is-active">
      <img src="imgs/piatto.png" />
    </a>
    <a href="#riassunto" class="tab-button tab-button--img">
      <img src="imgs/coperto.png" />
    </a>
  </nav>
</body>
</html>
